package ar.com.uade.impl;

import ar.com.uade.tda.TDAPila;

public class TDAPilaImpl implements TDAPila {

	private int[] valoresPila;
	private int cantidad;
	
	@Override
	public void inicializar() {
		valoresPila = new int[100];
		cantidad = 0;
	}

	@Override
	public void apilar(int valor) {
		valoresPila[cantidad] = valor;
		cantidad++;

	}

	@Override
	public void desapilar() {
		cantidad--;

	}

	@Override
	public boolean pilaVacia() {
		return cantidad == 0;
	}

	@Override
	public int tope() {
		return valoresPila[cantidad - 1];
	}

}
