package ar.com.uade.tda;

public interface TDAPila {
	
	public void inicializar();
	
	/** 
	 * Debe estar inicializada
	 *  */
	public void apilar(int valor);
	
	/** 
	 * Debe estar inicializada y no vacia
	 *  */
	public void desapilar();
	
	/** 
	 * Debe estar inicializada
	 *  */
	public boolean pilaVacia();
	
	/** 
	 * Debe estar inicializada y no vacia
	 *  */
	public int tope();
}
