package ar.com.uade.algoritmos;

import ar.com.uade.impl.TDAPilaImpl;
import ar.com.uade.tda.TDAPila;

public class EjercicioPila {

	public void pasarPila(TDAPila origen, TDAPila destino){
		
		while (!origen.pilaVacia()){
			destino.apilar(origen.tope());
			origen.desapilar();
		}	
	}
	
	public void copiarPila(TDAPila origen, TDAPila destino){
		
		TDAPila aux = new TDAPilaImpl();
		
		while (!origen.pilaVacia()){
			aux.apilar(origen.tope());
			origen.desapilar();
		}
		
		while (!aux.pilaVacia()){
			destino.apilar(aux.tope());
			origen.apilar(aux.tope());
			aux.desapilar();
		}
	}
	
	public void invertirPila(TDAPila pila){
		
		TDAPila aux1 = new TDAPilaImpl();
		TDAPila aux2 = new TDAPilaImpl();
		
		while (!pila.pilaVacia()){
			aux1.apilar(pila.tope());
			pila.desapilar();
		}
		
		while (!aux1.pilaVacia()){
			aux2.apilar(aux1.tope());
			aux1.desapilar();
		}
		
		while (!aux2.pilaVacia()){
			pila.apilar(aux2.tope());
			aux2.desapilar();
		}
	}
	
	public int contarElementosPila(TDAPila pila){
		
		int contador = 0;
		
		while (!pila.pilaVacia()){
			contador++;
			pila.desapilar();
		}
		
		return contador;
	}
	
	public int sumarElementosPila(TDAPila pila){
		
		int sumador = 0;
		
		while(!pila.pilaVacia()){
			sumador+=pila.tope();
			pila.desapilar();
		}
		
		return sumador;
	}
	
	public float promediarElementosPila(TDAPila pila){
		
		int sumador = 0;
		int contador = 0;
		
		while (!pila.pilaVacia()){
			contador++;
			sumador+=pila.tope();
			pila.desapilar();
		}
		
		return (float)sumador/contador;
	}
}
